import { createUrl, http } from "@/utils/http";
import type { Result } from "@/model";

// 后端请求接口
const apiUrl: any = createUrl({
  listApp: "/website/docapp/list",
  listDocTree: "/website/docinfo/tree",
  getDocDetail: "/website/docinfo/detail"
});

interface DocApp {
  id: number;
  appName: string;
}

/**
 * 接口管理
 */
export const api: any = {
  /**
   * 分页查询
   */
  listApp(): Promise<Result<Array<DocApp>>> {
    return http.get<Result<Array<DocApp>>, any>(apiUrl.listApp, {});
  },
  /**
   * 查询文档树
   * @param data
   */
  listDocTree(params: object) {
    return http.get<Result<Array<any>>, any>(apiUrl.listDocTree, { params });
  },
  /**
   * 查询文档详情
   * @param data
   */
  getDocDetail(params: object) {
    return http.get<Result<any>, any>(apiUrl.getDocDetail, { params });
  }
};
