import mdView from "./index.vue";
import { withInstall } from "@pureadmin/utils";

const MarkdownEditor = withInstall(mdView);

export default MarkdownEditor;
