import apiParamTable from "@/components/ApiParamTable/index.vue";
import { withInstall } from "@pureadmin/utils";

const ApiParamTable = withInstall(apiParamTable);

export { ApiParamTable };
