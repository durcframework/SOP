package com.gitee.sop.website.controller.website;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebsiteHomeController {

    private static final String REDIRECT_INDEX = "forward:index.html";

    // 后台admin入口地址
    @GetMapping("/")
    public String index() {
        return REDIRECT_INDEX;
    }

}
