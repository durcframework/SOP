package com.gitee.sop.website.common.config;

public interface IConfig {

    String getConfig(String key);

    String getConfig(String key, String defaultValue);

}
