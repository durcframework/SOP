package com.gitee.sop.website.service.doc;

import com.gitee.fastmybatis.core.support.LambdaService;
import com.gitee.sop.website.dao.entity.DocContent;
import com.gitee.sop.website.dao.mapper.DocContentMapper;
import org.springframework.stereotype.Service;


/**
 * @author 六如
 */
@Service
public class DocContentService implements LambdaService<DocContent, DocContentMapper> {

    public String getContent(Long docInfoId) {
        return this.query()
                .eq(DocContent::getDocInfoId, docInfoId)
                .getValueOptional(DocContent::getContent)
                .orElse("");
    }


}
