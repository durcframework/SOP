package com.gitee.sop.website.service.doc;

import com.alibaba.fastjson2.JSON;
import com.gitee.fastmybatis.core.support.LambdaService;
import com.gitee.sop.website.common.constants.YesOrNo;
import com.gitee.sop.website.dao.entity.DocInfo;
import com.gitee.sop.website.dao.mapper.DocInfoMapper;
import com.gitee.sop.website.service.doc.dto.torna.TornaDocInfoViewDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author 六如
 */
@Service
public class DocInfoService implements LambdaService<DocInfo, DocInfoMapper> {

    @Autowired
    private DocContentService docContentService;


    public TornaDocInfoViewDTO getDocDetail(Long id) {
        DocInfo docInfo = this.getById(id);
        if (docInfo == null || !YesOrNo.yes(docInfo.getIsPublish())) {
            throw new IllegalArgumentException("文档不存在");
        }
        String content = docContentService.getContent(docInfo.getId());
        return JSON.parseObject(content, TornaDocInfoViewDTO.class);
    }


}
