package com.gitee.sop.website.service.doc.dto;

import lombok.Data;

/**
 * @author 六如
 */
@Data
public class DocSettingDTO {

    private String tornaServerAddr;
    private String openProdUrl;
    private String openSandboxUrl;


}
