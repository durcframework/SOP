package com.gitee.sop.website.service.doc.dto;

import com.gitee.sop.website.service.doc.dto.torna.TornaDocInfoViewDTO;
import lombok.Data;

/**
 * @author 六如
 */
@Data
public class DocInfoViewDTO {

    private TornaDocInfoViewDTO docInfoView;

    private DocInfoConfigDTO docInfoConfig;

}
