package com.gitee.sop.website.service.doc;

import com.gitee.fastmybatis.core.support.LambdaService;
import com.gitee.sop.website.dao.entity.DocApp;
import com.gitee.sop.website.dao.mapper.DocAppMapper;
import org.springframework.stereotype.Service;


/**
 * @author 六如
 */
@Service
public class DocAppService implements LambdaService<DocApp, DocAppMapper> {

}
