package com.sop.example.rest.examplerest.rest;

import com.gitee.sop.support.annotation.Open;
import com.gitee.sop.support.annotation.OpenGroup;
import com.sop.example.rest.examplerest.rest.vo.GoodsVO;

import javax.validation.constraints.NotNull;

/**
 * @author 六如
 */
@OpenGroup("goods")
public interface GoodsController {

    @Open("/getGoodsById")
    GoodsVO getById(@NotNull(message = "id必填") Integer id);

}
