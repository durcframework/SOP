package com.sop.example.rest.examplerest.rest.vo;

import lombok.Data;

/**
 * @author 六如
 */
@Data
public class GoodsVO {

    private Integer id;

    private String name;

    private String remark;

}
