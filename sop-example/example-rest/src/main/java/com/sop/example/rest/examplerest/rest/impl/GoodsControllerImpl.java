package com.sop.example.rest.examplerest.rest.impl;

import com.gitee.sop.support.context.WebContext;
import com.sop.example.rest.examplerest.rest.GoodsController;
import com.sop.example.rest.examplerest.rest.vo.GoodsVO;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author 六如
 */
@DubboService(validation = "true")
public class GoodsControllerImpl implements GoodsController {
    @Override
    public GoodsVO getById(Integer id) {
        WebContext webContext = WebContext.current();
        GoodsVO goodsVO = new GoodsVO();
        goodsVO.setId(id);
        goodsVO.setName("冰箱");

        List<String> list = new ArrayList<>();
        // 获取真实地址
        list.add("realIp:" + webContext.getRealIp());
        // 获取请求头
        Map<String, String> headers = webContext.getHeaders();
        String token = headers.get("token");
        list.add("token:" + token);
        // 获取请求参数
        String idValue = webContext.getParameter("id");
        list.add("id:" + idValue);
        goodsVO.setRemark(list.toString());

        System.out.println(list);
        return goodsVO;
    }
}
