package com.gitee.sop.payment.open.impl;

import com.gitee.sop.payment.open.OpenPayment;
import com.gitee.sop.payment.open.req.PayOrderSearchRequest;
import com.gitee.sop.payment.open.req.PayTradeWapPayRequest;
import com.gitee.sop.payment.open.resp.PayOrderSearchResponse;
import com.gitee.sop.payment.open.resp.PayTradeWapPayResponse;
import java.util.UUID;

import com.gitee.sop.story.api.ProductService;
import com.gitee.sop.story.api.resp.ProductResult;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Value;


/**
 * 开放接口实现
 *
 * @author 六如
 */
@DubboService(validation = "true")
public class OpenPaymentImpl implements OpenPayment {

    @DubboReference
    private ProductService storyService;

    @Value("${dubbo.labels:}")
    private String env;


    @Override
    public PayTradeWapPayResponse tradeWapPay(PayTradeWapPayRequest request) {
        PayTradeWapPayResponse payTradeWapPayResponse = new PayTradeWapPayResponse();
        payTradeWapPayResponse.setPageRedirectionData(UUID.randomUUID().toString());
        return payTradeWapPayResponse;
    }

    @Override
    public PayOrderSearchResponse orderSearch(PayOrderSearchRequest request) {
        PayOrderSearchResponse payOrderSearchResponse = new PayOrderSearchResponse();
        payOrderSearchResponse.setOrderNo(request.getOrderNo());
        payOrderSearchResponse.setPayNo("xxxx");
        payOrderSearchResponse.setPayUserId(111L);
        payOrderSearchResponse.setPayUserName("Jim");

        try {
            ProductResult storyResult = storyService.getById(1L);
            payOrderSearchResponse.setRemark(storyResult + ",env:" + env);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return payOrderSearchResponse;
    }
}
