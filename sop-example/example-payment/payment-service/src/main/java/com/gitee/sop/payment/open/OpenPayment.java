package com.gitee.sop.payment.open;

import com.gitee.sop.payment.open.req.PayOrderSearchRequest;
import com.gitee.sop.payment.open.req.PayTradeWapPayRequest;
import com.gitee.sop.payment.open.resp.PayOrderSearchResponse;
import com.gitee.sop.payment.open.resp.PayTradeWapPayResponse;
import com.gitee.sop.support.annotation.Open;

/**
 * 支付接口
 *
 * @author 六如
 */
public interface OpenPayment {

    /**
     * 手机网站支付接口
     *
     * @apiNote 该接口是页面跳转接口，用于生成用户访问跳转链接。
     * 请在服务端执行SDK中pageExecute方法，读取响应中的body()结果。
     * 该结果用于跳转到页面，返回到用户浏览器渲染或重定向跳转到页面。
     * 具体使用方法请参考 <a href="https://torna.cn" target="_blank">接入指南</a>
     */
    @Open("pay.trade.wap.pay")
    PayTradeWapPayResponse tradeWapPay(PayTradeWapPayRequest request);


    /**
     * 订单查询接口
     *
     * @param request
     * @return
     */
    @Open("pay.order.search")
    PayOrderSearchResponse orderSearch(PayOrderSearchRequest request);

}
