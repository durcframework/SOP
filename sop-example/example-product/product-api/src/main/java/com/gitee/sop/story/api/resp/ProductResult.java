package com.gitee.sop.story.api.resp;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 六如
 */
public class ProductResult implements Serializable {
    private static final long serialVersionUID = -3743413007549072654L;

    private Integer id;

    private String name;

    // 日期格式要用Date,暂不支持LocalDateTime
    private Date addTime = new Date();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    @Override
    public String toString() {
        return "StoryResult{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", addTime=" + addTime +
                '}';
    }
}