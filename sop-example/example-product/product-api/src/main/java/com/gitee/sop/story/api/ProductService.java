package com.gitee.sop.story.api;

import com.gitee.sop.story.api.resp.ProductResult;

/**
 * @author 六如
 */
public interface ProductService {

    ProductResult getById(Long id);

}
