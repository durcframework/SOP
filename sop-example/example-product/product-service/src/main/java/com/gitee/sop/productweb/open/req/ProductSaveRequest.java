package com.gitee.sop.productweb.open.req;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author 六如
 */
@Data
public class ProductSaveRequest implements Serializable {
    private static final long serialVersionUID = -1214422742659231037L;

    /**
     * 产品名称
     */
    @NotBlank(message = "产品名称必填")
    @Length(max = 64)
    private String productName;

    /**
     * 添加时间
     */
    @NotNull(message = "添加时间必填")
    private Date addTime;

}
