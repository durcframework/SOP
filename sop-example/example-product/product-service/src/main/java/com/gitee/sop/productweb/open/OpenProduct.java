package com.gitee.sop.productweb.open;

import com.gitee.sop.productweb.open.req.ProductSaveRequest;
import com.gitee.sop.productweb.open.resp.ProductResponse;
import com.gitee.sop.support.annotation.Open;
import com.gitee.sop.support.context.OpenContext;
import com.gitee.sop.support.dto.FileData;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 产品服务
 *
 * @author 六如
 * @dubbo
 */
public interface OpenProduct {

    /**
     * 新增故事
     *
     * @param request 入参
     * @return 返回id
     */
    @Open("product.save")
    Integer save(ProductSaveRequest request);

    @Open("product.update")
    Integer update(Integer id, ProductSaveRequest request);

    // 演示抛出异常
    @Open("product.updateError")
    Integer updateError(Integer id);

    @Open("product.get")
    ProductResponse getById(@NotNull(message = "id必填") Integer id);

    // 需要授权
    @Open(value = "product.get", version = "2.0", permission = true)
    ProductResponse getByIdV2(Long id);

    @Open(value = "product.get.context")
    ProductResponse getContext(Long id, OpenContext context);


    // 默认方法,注解放在这里也有效
    @Open("product.find")
    default ProductResponse getById(Integer id, String name) {
        ProductResponse storyResponse = new ProductResponse();
        storyResponse.setId(id);
        storyResponse.setName(name);
        return storyResponse;
    }

    // 默认方法,注解放在这里也有效
    @Open("alipay.story.find")
    default ProductResponse findByName(String name) {
        ProductResponse storyResponse = new ProductResponse();
        storyResponse.setName(name);
        return storyResponse;
    }

    // 演示单文件上传
    @Open("product.upload")
    ProductResponse upload(ProductSaveRequest request, FileData file);

    // 演示多文件上传
    @Open("product.upload.more")
    ProductResponse upload2(
            ProductSaveRequest request,
            @NotNull(message = "身份证正面必填") FileData idCardFront,
            @NotNull(message = "身份证背面必填") FileData idCardBack
    );

    // 演示多文件上传
    @Open("product.upload.list")
    ProductResponse upload3(
            ProductSaveRequest request,
            @Size(min = 2, message = "最少上传2个文件")
            List<FileData> files
    );

    // 下载
    @Open("product.download")
    FileData download(Integer id);
}
