package com.gitee.sop.productweb.rpc;

import com.gitee.sop.story.api.ProductService;
import com.gitee.sop.story.api.resp.ProductResult;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Value;

import java.util.Date;


/**
 * @author 六如
 */
@DubboService
public class ProductServiceImpl implements ProductService {
    @Value("${dubbo.labels:}")
    private String env;

    @Override
    public ProductResult getById(Long id) {
        System.out.println("StoryService.getById, env=" + env);
        ProductResult storyResult = new ProductResult();
        storyResult.setName("彩电-" + env);
        storyResult.setId(id.intValue());
        storyResult.setAddTime(new Date());
        return storyResult;
    }
}
