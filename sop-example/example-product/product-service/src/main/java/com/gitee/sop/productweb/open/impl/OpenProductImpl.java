package com.gitee.sop.productweb.open.impl;

import com.gitee.sop.productweb.message.StoryMessageEnum;
import com.gitee.sop.productweb.open.OpenProduct;
import com.gitee.sop.productweb.open.req.ProductSaveRequest;
import com.gitee.sop.productweb.open.resp.ProductResponse;
import com.gitee.sop.support.context.OpenContext;
import com.gitee.sop.support.dto.CommonFileData;
import com.gitee.sop.support.dto.FileData;
import com.gitee.sop.support.exception.OpenException;
import org.apache.commons.io.IOUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;


/**
 * 开放接口实现
 *
 * @author 六如
 */
@DubboService(validation = "true")
public class OpenProductImpl implements OpenProduct {

    @Value("${dubbo.labels:}")
    private String env;


    @Override
    public Integer save(ProductSaveRequest request) {
        return 1;
    }


    @Override
    public Integer update(Integer id, ProductSaveRequest request) {
        System.out.println("update, id:" + id + ", storySaveDTO=" + request);
        return 1;
    }

    @Override
    public Integer updateError(Integer id) {
        // 抛出业务异常
        if (id == null || id == 0) {
            throw new OpenException(StoryMessageEnum.ISV_PARAM_ERROR, "id is null or 0");
        }
        if (id < 0) {
            // 自定义code, msg
            throw new OpenException("4000", "id必须大于0");
        }
        return 1;
    }

    @Override
    public ProductResponse getById(Integer id) {
        ProductResponse storyResponse = new ProductResponse();
        storyResponse.setId(id);
        storyResponse.setName("冰箱-" + env);
        return storyResponse;
    }


    @Override
    public ProductResponse getByIdV2(Long id) {
        ProductResponse storyResponse = new ProductResponse();
        storyResponse.setId(2);
        storyResponse.setName("冰箱2.0");
        return storyResponse;
    }

    // 演示获取上下文
    @Override
    public ProductResponse getContext(Long id, OpenContext context) {
        ProductResponse storyResponse = new ProductResponse();
        storyResponse.setId(3);
        storyResponse.setName(context.toString());
        // 获取回调参数
        String notifyUrl = context.getNotifyUrl();
        System.out.println(notifyUrl);

        // 方式2：使用OpenContext.current()
        String notifyUrl1 = OpenContext.current().getNotifyUrl();
        System.out.println(Objects.equals(notifyUrl1, notifyUrl));

        return storyResponse;
    }

    @Override
    public ProductResponse upload(ProductSaveRequest storySaveDTO, FileData file) {
        System.out.println("getName:" + file.getName());
        System.out.println("getOriginalFilename:" + file.getOriginalFilename());
        checkFile(Arrays.asList(file));

        ProductResponse storyResponse = new ProductResponse();
        storyResponse.setId(1);
        storyResponse.setName(file.getOriginalFilename());
        return storyResponse;
    }


    @Override
    public ProductResponse upload2(ProductSaveRequest storySaveDTO, FileData idCardFront, FileData idCardBack) {
        System.out.println("upload:" + storySaveDTO);
        checkFile(Arrays.asList(idCardFront, idCardBack));

        ProductResponse storyResponse = new ProductResponse();
        storyResponse.setId(1);
        storyResponse.setName(storySaveDTO.getProductName());
        return storyResponse;
    }

    @Override
    public ProductResponse upload3(ProductSaveRequest storySaveDTO, List<FileData> files) {
        List<String> list = new ArrayList<>();
        list.add("upload:" + storySaveDTO);
        checkFile(files);

        ProductResponse storyResponse = new ProductResponse();
        storyResponse.setId(1);
        storyResponse.setName(storySaveDTO.getProductName());
        return storyResponse;
    }

    @Override
    public FileData download(Integer id) {
        CommonFileData fileData = new CommonFileData();
        ClassPathResource resource = new ClassPathResource("download.txt");
        fileData.setOriginalFilename(resource.getFilename());
        try {
            fileData.setData(IOUtils.toByteArray(resource.getInputStream()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return fileData;
    }

    private void checkFile(List<FileData> fileDataList) {
        for (FileData file : fileDataList) {
            Assert.notNull(file.getName());
            Assert.notNull(file.getOriginalFilename());
            Assert.notNull(file.getBytes());
            Assert.isTrue(!file.isEmpty());
        }
    }
}
