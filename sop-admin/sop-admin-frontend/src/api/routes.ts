import { createUrl, http } from "@/utils/http";

// 后端请求接口
const apiUrl: any = createUrl({
  listMenu: "sys/userperm/listCurrentUserMenu"
});

type Result = {
  success: boolean;
  data: Array<any>;
};

export const getAsyncRoutes = () => {
  return http.request<Result>("get", apiUrl.listMenu);
  // return http.request<Result>("get", "/get-async-routes");
};
