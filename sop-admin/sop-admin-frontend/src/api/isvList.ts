import { createUrl, http } from "@/utils/http";
import type { PageResult, Result } from "@/model";

// 后端请求接口
const apiUrl: any = createUrl({
  page: "/isv/page",
  add: "/isv/add",
  update: "/isv/update",
  del: "/isv/delete",
  getKeys: "/isv/getKeys",
  updateStatus: "/isv/updateStatus",
  createKeys: "/isv/createKeys",
  updateKeys: "/isv/updateKeys",
  listGroup: "perm/isv/group/listIsvGroupId",
  updateGroup: "perm/isv/group/setting"
});

/*
private String publicKey;
private String privateKey;
 */
export interface KeyStore {
  publicKey: string;
  privateKey: string;
}

/**
 * 接口管理
 */
export const api: any = {
  /**
   * 分页查询
   * @param params 查询参数
   */
  page(params: object): Promise<PageResult> {
    return http.get<PageResult, any>(apiUrl.page, { params });
  },
  /**
   * 新增
   * @param data 表单内容
   */
  add(data: object) {
    return http.post<Result<any>, any>(apiUrl.add, { data });
  },
  /**
   * 修改
   * @param data 表单内容
   */
  update(data: object) {
    return http.post<Result<any>, any>(apiUrl.update, { data });
  },
  /**
   * 删除
   * @param data 表单内容
   */
  del(data: object) {
    return http.post<Result<any>, any>(apiUrl.del, { data });
  },
  /**
   * 查看秘钥
   * @param params
   */
  viewKeys(params: object) {
    return http.get<Result<any>, any>(apiUrl.getKeys, { params });
  },
  /**
   * 修改状态
   * @param data 表单内容
   */
  updateStatus(data: object) {
    return http.post<Result<any>, any>(apiUrl.updateStatus, { data });
  },
  /**
   * 创建秘钥
   * @param keyFormat 秘钥格式，1：PKCS8(JAVA适用)，2：PKCS1(非JAVA适用)
   */
  createKeys(keyFormat): Promise<Result<KeyStore>> {
    const data = {
      keyFormat: keyFormat
    };
    return http.post<Result<KeyStore>, any>(apiUrl.createKeys, { data });
  },
  /**
   * 修改秘钥
   * @param data 表单内容
   */
  updateKeys(data: object): Promise<Result<KeyStore>> {
    return http.post<Result<any>, any>(apiUrl.updateKeys, { data });
  },
  /**
   * 修改秘钥
   * @param data 表单内容
   */
  listGroup(isvId: Number): Promise<Result<string>> {
    const data = {
      isvId: isvId
    };
    return http.get<Result<string>, any>(apiUrl.listGroup, { params: data });
  },
  /**
   * 设置分组
   * @param isvId isvId
   * @param groupCodes groupCodes
   */
  updateGroup(
    isvId: Number,
    groupIds: Array<number>
  ): Promise<Result<KeyStore>> {
    const data = {
      isvId: isvId,
      groupIds: groupIds
    };
    return http.post<Result<any>, any>(apiUrl.updateGroup, { data });
  }
};
