import { createUrl, http } from "@/utils/http";
import type { PageResult, Result } from "@/model";

// 后端请求接口
const apiUrl: any = createUrl({
  page: "/perm/group/page",
  add: "/perm/group/add",
  update: "/perm/group/update",
  del: "/perm/group/delete",
  listAll: "/perm/group/listAll"
});

/**
 * 接口管理
 */
export const api: any = {
  /**
   * 分页查询
   * @param params 查询参数
   */
  page(params: object): Promise<PageResult> {
    return http.get<PageResult, any>(apiUrl.page, { params });
  },
  /**
   * 查询全部
   */
  listAll(): Promise<Result<any>> {
    return http.get<Result<any>, any>(apiUrl.listAll, {});
  },
  /**
   * 新增
   * @param data 表单内容
   */
  add(data: object) {
    return http.post<Result<any>, any>(apiUrl.add, { data });
  },
  /**
   * 修改
   * @param data 表单内容
   */
  update(data: object) {
    return http.post<Result<any>, any>(apiUrl.update, { data });
  },
  /**
   * 删除
   * @param data 表单内容
   */
  del(data: object) {
    return http.post<Result<any>, any>(apiUrl.del, { data });
  }
};
