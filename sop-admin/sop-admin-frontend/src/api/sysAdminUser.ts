import { createUrl, http } from "@/utils/http";
import type { PageResult, Result } from "@/model";

// 后端请求接口
const apiUrl: any = createUrl({
  page: "/sys/user/page",
  add: "/sys/user/add",
  update: "/sys/user/update",
  del: "/sys/user/delete"
});

/**
 * 接口管理
 */
export const api: any = {
  /**
   * 分页查询
   * @param params 查询参数
   */
  page(params: object): Promise<PageResult> {
    return http.get<PageResult, any>(apiUrl.page, { params });
  },
  /**
   * 新增
   * @param data 表单内容
   */
  add(data: object) {
    return http.post<Result<any>, any>(apiUrl.add, { data });
  },
  /**
   * 修改
   * @param data 表单内容
   */
  update(data: object) {
    return http.post<Result<any>, any>(apiUrl.update, { data });
  },
  /**
   * 删除
   * @param data 表单内容
   */
  del(data: object) {
    return http.post<Result<any>, any>(apiUrl.del, { data });
  }
};
