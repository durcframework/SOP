export enum YesOrNoEnum {
  NO = 0,
  YES = 1
}

export enum StatusEnum {
  ENABLE = 1,
  DISABLE = 2
}

export enum KeyFormatEnum {
  PKCS8 = 1,
  PKCS1 = 2
}

export enum RegSource {
  SYS = 1,
  CUSTOM = 2
}
