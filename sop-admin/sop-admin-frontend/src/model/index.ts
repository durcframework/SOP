export interface Result<T> {
  success: boolean;
  data: T;
  msg: "";
  code: "";
}

export interface PageResult {
  success: boolean;
  msg: "";
  code: "";
  data: {
    total: 0;
    list: Array<any>;
  };
}
