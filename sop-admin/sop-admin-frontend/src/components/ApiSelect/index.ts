import { ref } from "vue";
import { type PageInfo, type PlusColumn, useTable } from "plus-pro-components";
import { withInstall } from "@pureadmin/utils";
import apiSelect from "./index.vue";
import { StatusEnum } from "@/model/enums";
import { api as serveApi } from "@/api/serveApi";

export const dlgApiSelectShow = ref(false);
export const dlgWidth = ref(1100);
export const handleSaveApi = ref((_: Array<number>) => {});

// ========= search form =========

// 查询表单对象
export const searchFormData = ref({
  apiName: "",
  status: "",
  isPermission: null,
  pageIndex: 1,
  pageSize: 10
});

// 查询表单字段定义
export const searchFormColumns: PlusColumn[] = [
  {
    label: "接口名称",
    prop: "apiName"
  },
  {
    label: "状态",
    prop: "status",
    width: 80,
    valueType: "select",
    options: [
      {
        label: "启用",
        value: StatusEnum.ENABLE,
        color: "green"
      },
      {
        label: "禁用",
        value: StatusEnum.DISABLE,
        color: "red"
      }
    ]
  }
];

// ========= table =========

// 表格对象
export const { tableData, total, pageInfo } = useTable<any[]>();
// 默认每页条数,默认10
pageInfo.value.pageSize = 10;

const multipleSelection = ref([]);
export const selectable = (row: any) => row.status === StatusEnum.ENABLE;
// 表格字段定义
export const tableColumns: PlusColumn[] = [
  {
    label: "所属应用",
    prop: "application",
    tableColumnProps: {
      showOverflowTooltip: true
    }
  },
  {
    label: "接口名称",
    prop: "apiName",
    tableColumnProps: {
      showOverflowTooltip: true
    }
  },
  {
    label: "版本号",
    prop: "apiVersion",
    width: 80
  },
  {
    label: "接口描述",
    prop: "description",
    tableColumnProps: {
      showOverflowTooltip: true
    }
  },
  {
    label: "注册来源",
    prop: "regSource",
    width: 100,
    valueType: "select",
    options: [
      {
        label: "系统",
        value: 1,
        color: "blue"
      },
      {
        label: "手动",
        value: 2,
        color: "green"
      }
    ]
  },
  {
    label: "状态",
    prop: "status",
    width: 80,
    valueType: "select",
    options: [
      {
        label: "启用",
        value: StatusEnum.ENABLE,
        color: "green"
      },
      {
        label: "禁用",
        value: StatusEnum.DISABLE,
        color: "red"
      }
    ]
  }
];

export const openDlg = () => {
  dlgApiSelectShow.value = true;
  search();
};

export const handleConfirm = () => {
  const idList = multipleSelection.value.map(row => row.id);
  handleSaveApi.value(idList);
};

export const handleClose = () => {
  dlgApiSelectShow.value = false;
};

export const handleSelectionChange = (rows: any) => {
  multipleSelection.value = rows;
};

// 点击查询按钮
export const handleSearch = () => {
  search();
};

// 分页事件
export const handlePaginationChange = (_pageInfo: PageInfo): void => {
  pageInfo.value = _pageInfo;
  search();
};

// 查询
export const search = async () => {
  try {
    const { data } = await doSearch();
    tableData.value = data.list;
    total.value = data.total;
  } catch (error) {}
};
// 请求接口
const doSearch = async () => {
  // 查询参数
  const data = searchFormData.value;
  // 添加分页参数
  data.pageIndex = pageInfo.value.page;
  data.pageSize = pageInfo.value.pageSize;

  return serveApi.page(data);
};

// 页面加载
search();

const ApiSelect = withInstall(apiSelect);

export { ApiSelect };
