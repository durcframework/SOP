// options推荐写法
// 1. 定义一个 `ref`数组
import { computed, type Ref, ref } from "vue";
import { type OptionsRow, type PlusColumn } from "plus-pro-components";
import { api } from "@/api/isvList";
import { api as groupApi } from "@/api/permGroup";
import { ElMessage } from "element-plus";
import { useIsvList } from "@/views/isv/list/index";

const { search } = useIsvList();

// 弹窗显示
export const dlgGroupSetting = ref(false);
export const settingGroupFormData = ref<any>({
  isvId: 0,
  groupIds: []
});

const groupList: Ref<OptionsRow[]> = ref([]);
// 2. 异步函数获取到值赋值到 `ref`
const loadGroup = () => {
  groupApi.listAll().then(resp => {
    const rows = resp.data;
    /*
     options.value = [
    { label: '未解决', value: '0', color: 'red' },
    { label: '已解决', value: '1', color: 'blue' }
  ]
     */
    groupList.value = rows.map(row => {
      return {
        label: row.groupName,
        value: row.id
      };
    });
  });
};
loadGroup();

export const groupColumns: PlusColumn[] = [
  {
    label: "分组",
    width: 120,
    prop: "groupIds",
    valueType: "checkbox",
    // options推荐写法
    // 3. 用 computed 返回 ref 的 value
    options: computed(() => groupList.value)
  }
];

export const settingGroup = (row: any) => {
  api.listGroup(row.id).then(resp => {
    settingGroupFormData.value = {
      isvId: row.id,
      groupIds: resp.data
    };
    dlgGroupSetting.value = true;
  });
};

export const handleUpdateGroup = () => {
  const data = settingGroupFormData.value;

  api.updateGroup(data.isvId, data.groupIds).then(() => {
    ElMessage.success("保存成功");
    dlgGroupSetting.value = false;
    search();
  });
};
