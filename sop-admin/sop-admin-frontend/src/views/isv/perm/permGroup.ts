import { onMounted, ref } from "vue";
import {
  type ButtonsCallBackParams,
  type PlusColumn,
  useTable
} from "plus-pro-components";
import { ElMessage } from "element-plus";
import { api } from "@/api/permGroup";
import { searchTable } from "@/views/isv/perm/permGroupApi";

export function usePermGroup() {
  const isAdd = ref(false);

  // ========= search form =========

  // 查询表单对象
  const searchFormData = ref({
    groupName: ""
  });

  // 查询表单字段定义
  const searchFormColumns: PlusColumn[] = [
    {
      label: "分组描述",
      prop: "groupName"
    }
  ];

  // ========= table =========

  // 表格对象
  const { tableData, buttons: actionButtons } = useTable<any[]>();

  const selectedGroupId = ref(0);

  // 表格字段定义
  const tableColumns: PlusColumn[] = [
    {
      label: "分组名称",
      prop: "groupName"
    }
  ];
  // 表格按钮定义
  actionButtons.value = [
    {
      // 修改
      text: "修改",
      code: "edit",
      props: {
        type: "primary"
      },
      onClick(params: ButtonsCallBackParams) {
        params.e.stopPropagation();
        isAdd.value = false;
        editFormData.value = Object.assign({}, params.row);
        dlgTitle.value = "修改";
        dlgShow.value = true;
      }
    },
    {
      // 删除
      text: "删除",
      code: "delete",
      props: {
        type: "danger"
      },
      confirm: {
        options: { draggable: false }
      },
      onConfirm(params: ButtonsCallBackParams) {
        params.e.stopPropagation();
        api.del(params.row).then(() => {
          ElMessage({
            message: "删除成功",
            type: "success"
          });
          dlgShow.value = false;
          search();
        });
      }
    }
  ];

  // ========= dialog form =========

  // 弹窗显示
  const dlgShow = ref(false);
  const dlgTitle = ref("");
  // 表单值
  const editFormDataGen = () => {
    return {
      groupName: ""
    };
  };
  const editFormData = ref<any>(editFormDataGen());
  const editFormRules = {
    groupName: [{ required: true, message: "请输入分组名称" }]
  };

  // 表单内容
  const editFormColumns: PlusColumn[] = [
    {
      label: "分组名称",
      prop: "groupName",
      valueType: "input"
    }
  ];

  // ========= event =========

  // 点击行
  const handleRowClick = row => {
    const groupId = row.id;
    selectedGroupId.value = groupId;
    searchTable(groupId);
  };

  // 添加按钮事件
  const handleAdd = () => {
    isAdd.value = true;
    editFormData.value = editFormDataGen();
    dlgTitle.value = "新增";
    dlgShow.value = true;
  };

  // 保存按钮事件,校验成功后触发
  const handleSave = () => {
    const postData = editFormData.value;
    const pms = isAdd.value ? api.add(postData) : api.update(postData);
    pms.then(() => {
      ElMessage.success("保存成功");
      dlgShow.value = false;
      search();
    });
  };

  // 点击查询按钮
  const handleSearch = () => {
    search();
  };

  // 查询
  const search = async () => {
    try {
      const { data } = await doSearch();
      tableData.value = data;
    } catch (error) {}
  };
  // 请求接口
  const doSearch = async () => {
    // 查询参数
    const data = searchFormData.value;
    return api.listAll(data);
  };

  // 页面加载
  search();

  onMounted(() => {
    search();
  });

  return {
    actionButtons,
    dlgShow,
    dlgTitle,
    editFormColumns,
    editFormData,
    editFormRules,
    handleAdd,
    handleRowClick,
    handleSave,
    handleSearch,
    searchFormColumns,
    searchFormData,
    selectedGroupId,
    tableColumns,
    tableData
  };
}
