package com.gitee.gen.controller;


import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.ModelAndView;

@Controller
public class HomeController {

    private static final ModelAndView INDEX_VIEW = new ModelAndView("index.html");

    @Get
    @Mapping("/")
    public void index(Context context) {
        context.redirect("index.html");
    }

    @Mapping("/index")
    public ModelAndView index2() {
        return INDEX_VIEW;
    }
}
