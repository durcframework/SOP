package com.gitee.gen.service;

import com.gitee.gen.common.GeneratorParam;
import com.gitee.gen.entity.TemplateConfig;
import com.gitee.gen.gen.CodeFile;
import com.gitee.gen.gen.GeneratorConfig;
import org.apache.commons.io.FileUtils;
import org.noear.snack.core.utils.StringUtil;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author 六如
 */
@Component
public class LocalGenService {

    @Inject
    private GeneratorService generatorService;

    @Inject
    private TemplateConfigService templateConfigService;

    /**
     * 生成本地文件
     *
     * @param extConfig 配置
     */
    public void genLocal(Properties extConfig) throws IOException {
        check(extConfig);
        GeneratorParam generatorParam = buildGeneratorParam(extConfig);
        GeneratorConfig generatorConfig = buildGeneratorConfig(extConfig);
        List<CodeFile> files = generatorService.generate(generatorParam, generatorConfig, extConfig);
        for (CodeFile file : files) {
            FileUtils.writeStringToFile(new File(file.getFileName()), file.getContent(), StandardCharsets.UTF_8);
            System.out.println("生成文件：" + file.getFileName());
        }
    }

    private void check(Properties extConfig) {
        String moduleName = extConfig.getProperty("moduleName");
        if (StringUtil.isEmpty(moduleName)) {
            throw new IllegalArgumentException("moduleName 配置必填");
        }
    }

    private GeneratorParam buildGeneratorParam(Properties extConfig) {
        GeneratorParam generatorParam = new GeneratorParam();
        generatorParam.setDatasourceConfigId(0);

        String table = extConfig.getProperty("tables");
        if (table == null) {
            throw new RuntimeException("未指定表名");
        }
        String[] split = table.split(",");
        generatorParam.setTableNames(Stream.of(split).collect(Collectors.toList()));

        List<Integer> templateIds = templateConfigService.listByGroupName(extConfig.getProperty("groupName"))
                .stream()
                .map(TemplateConfig::getId)
                .collect(Collectors.toList());
        generatorParam.setTemplateConfigIdList(templateIds);
        generatorParam.setPackageName("");
        generatorParam.setDelPrefix("");
        generatorParam.setAuthor(extConfig.getProperty("author"));

        return generatorParam;
    }

    private GeneratorConfig buildGeneratorConfig(Properties extConfig) {
        GeneratorConfig generatorConfig = new GeneratorConfig();
        generatorConfig.setDbName(extConfig.getProperty("db.name"));
        String driverClass = extConfig.getProperty("db.driverClass");
        if (StringUtil.isEmpty(driverClass)) {
            throw new RuntimeException("未指定driverClass");
        }

        generatorConfig.setDbType(buildDbType(driverClass));
        generatorConfig.setPort(Integer.parseInt(extConfig.getProperty("db.port")));
        generatorConfig.setHost(extConfig.getProperty("db.host"));
        generatorConfig.setUsername(extConfig.getProperty("db.username"));
        generatorConfig.setPassword(extConfig.getProperty("db.password"));
        generatorConfig.setDriverClass(driverClass);
        return generatorConfig;
    }

    private Integer buildDbType(String driverClass) {
        if (driverClass.contains("mysql")) {
            return 1;
        }
        if (driverClass.contains("OracleDriver")) {
            return 2;
        }
        if (driverClass.contains("SQLServerDriver")) {
            return 3;
        }
        if (driverClass.contains("postgresql")) {
            return 4;
        }
        if (driverClass.contains("DmDriver")) {
            return 5;
        }

        return 1;
    }
}
