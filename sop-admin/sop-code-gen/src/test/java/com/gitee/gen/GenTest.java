package com.gitee.gen;

import com.gitee.gen.service.LocalGenService;
import org.junit.runner.RunWith;
import org.noear.solon.annotation.Inject;
import org.noear.solon.core.AppClassLoader;
import org.noear.solon.test.SolonJUnit4ClassRunner;
import org.noear.solon.test.SolonTest;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 文件生成到本地
 *
 * @author 六如
 */
@RunWith(SolonJUnit4ClassRunner.class)
@SolonTest(value = App.class, args = {"--server.port=9201"})
public class GenTest {

    @Inject
    private LocalGenService localGenService;

    /**
     * 生成文件
     * @param filename 配置文件，要求放在resources下
     */
    public void gen(String filename) {
        InputStream inputStream = AppClassLoader.getSystemResourceAsStream(filename);
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
            localGenService.genLocal(properties);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
