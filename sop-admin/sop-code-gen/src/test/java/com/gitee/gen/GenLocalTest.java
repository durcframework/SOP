package com.gitee.gen;

import org.junit.Test;

/**
 * 文件生成到本地项目中
 *
 * @author 六如
 */
public class GenLocalTest extends GenTest {

    @Test
    public void run() {
        this.gen("order_info.properties");
    }
}
