package com.gitee.sop.admin.service.sys.login.dto;

import lombok.Data;

/**
 * @author 六如
 */
@Data
public class LoginForm {
    private String username;
    private String password;
}
