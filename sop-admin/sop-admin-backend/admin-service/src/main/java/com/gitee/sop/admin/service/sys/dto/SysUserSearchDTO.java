package com.gitee.sop.admin.service.sys.dto;

import com.gitee.fastmybatis.core.query.Operator;
import com.gitee.fastmybatis.core.query.annotation.Condition;
import com.gitee.fastmybatis.core.query.param.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 备注：角色表
 *
 * @author 六如
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SysUserSearchDTO extends PageParam {
    private static final long serialVersionUID = 7794265174728302379L;

    /**
     * 用户名
     */
    @Condition(operator = Operator.like)
    private String username;

    /**
     * 邮箱
     */
    @Condition(operator = Operator.like)
    private String phone;


    /**
     * 状态，1：启用，2：禁用
     */
    @Condition
    private Integer status;

    @Condition(ignore = true)
    private Long deptId;


}
