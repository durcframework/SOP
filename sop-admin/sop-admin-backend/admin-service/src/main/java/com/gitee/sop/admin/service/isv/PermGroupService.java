package com.gitee.sop.admin.service.isv;

import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.query.LambdaQuery;
import com.gitee.sop.admin.common.exception.BizException;
import com.gitee.sop.admin.common.support.ServiceSupport;
import com.gitee.sop.admin.dao.entity.PermGroup;
import com.gitee.sop.admin.dao.entity.PermIsvGroup;
import com.gitee.sop.admin.dao.mapper.PermGroupMapper;
import com.gitee.sop.admin.dao.mapper.PermIsvGroupMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;


/**
 * @author 六如
 */
@Service
public class PermGroupService implements ServiceSupport<PermGroup, PermGroupMapper> {

    @Autowired
    PermIsvGroupMapper permIsvGroupMapper;

    public PageInfo<PermGroup> doPage(LambdaQuery<PermGroup> query) {
        query.orderByDesc(PermGroup::getId);
        PageInfo<PermGroup> page = this.page(query);

        // 格式转换
        return page.convert(isvInfo -> {

            return isvInfo;
        });
    }

    public Map<Long, String> getCodeNameMap(Collection<Long> groupIdList) {
        return this.query()
                .in(PermGroup::getId, groupIdList)
                .map(PermGroup::getId, PermGroup::getGroupName);
    }

    public int delete(Long groupId) {
        boolean used = permIsvGroupMapper.query()
                .eq(PermIsvGroup::getGroupId, groupId)
                .get() != null;
        if (used) {
            throw new BizException("无法删除:分组已被使用");
        }
        return this.deleteById(groupId);
    }


}
