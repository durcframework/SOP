package com.gitee.sop.admin.service.jackson.convert.annotation;


import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gitee.sop.admin.service.jackson.convert.serde.UserFormatSerializer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 序列化自动转换成用户名称
 *
 * @author 六如
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@JacksonAnnotationsInside
@JsonSerialize(using = UserFormatSerializer.class)
public @interface UserFormat {

    /**
     * 显示用户名称
     * @return
     */
    boolean showName() default true;


}
