package com.gitee.sop.admin.service.sys.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;


/**
 * @author 六如
 */
@Data
public class UserPermissionDTO {

    /**
     * 用户角色code
     */
    private List<String> roles;

    /**
     * 用户按钮级别code
     */
    private List<String> permissions;

    public static UserPermissionDTO empty() {
        UserPermissionDTO userPermissionDTO = new UserPermissionDTO();
        userPermissionDTO.setRoles(new ArrayList<>());
        userPermissionDTO.setPermissions(new ArrayList<>());
        return userPermissionDTO;
    }

}
