package com.gitee.sop.admin.service.doc.dto.torna;

import lombok.Data;

/**
 * @author 六如
 */
@Data
public class TornaModuleDTO {

    private Long id;

    private String name;

}
