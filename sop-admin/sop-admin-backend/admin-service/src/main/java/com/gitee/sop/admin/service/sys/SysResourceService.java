package com.gitee.sop.admin.service.sys;

import com.gitee.sop.admin.common.support.ServiceSupport;
import com.gitee.sop.admin.dao.entity.SysResource;
import com.gitee.sop.admin.dao.mapper.SysResourceMapper;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;


/**
 * @author 六如
 */
@Service
public class SysResourceService implements ServiceSupport<SysResource, SysResourceMapper> {

    public List<SysResource> listAllResource() {
        List<SysResource> list = this.listAll();
        list.sort(Comparator.comparing(SysResource::getRank));
        return list;
    }
}
