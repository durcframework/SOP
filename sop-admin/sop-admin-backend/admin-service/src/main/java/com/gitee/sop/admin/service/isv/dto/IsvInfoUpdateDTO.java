package com.gitee.sop.admin.service.isv.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author 六如
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class IsvInfoUpdateDTO extends IsvInfoAddDTO {
    private Long id;

}
