package com.gitee.sop.admin.service.isv.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author 六如
 */
@Data
public class IsvGroupSettingDTO {

    @NotNull
    private Long isvId;

    private List<Long> groupIds;

}
