package com.gitee.sop.admin.service.sys.login.dto;

import com.gitee.sop.admin.common.user.User;
import lombok.Data;

import java.util.List;

/**
 * @author 六如
 */
@Data
public class LoginUser implements User {

    /*
    avatar: "https://avatars.githubusercontent.com/u/44761321",
    username: "admin",
    nickname: "小铭",
    // 一个用户可能有多个角色
    roles: ["admin"],
    // 按钮级别权限
    permissions: ["*:*:*"],
    accessToken: "eyJhbGciOiJIUzUxMiJ9.admin",
    refreshToken: "eyJhbGciOiJIUzUxMiJ9.adminRefresh",
    expires: "2030/10/30 00:00:00"
     */

    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 用户名
     */
    private String nickname;

    /**
     * 密码
     */
    private String password;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 注册类型
     */
    private Integer regType;

    /**
     * 状态，1：启用，0：禁用
     */
    private Integer status;

    private List<String> roles;

    private List<String> permissions;

    private String accessToken;

    private String refreshToken;

    private String expires;

    @Override
    public Long getUserId() {
        return id;
    }

    @Override
    public Integer getStatus() {
        return status;
    }

    @Override
    public String getToken() {
        return accessToken;
    }
}
