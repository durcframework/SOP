package com.gitee.sop.admin.service.doc.dto;

import lombok.Data;

/**
 * @author 六如
 */
@Data
public class DocInfoConfigDTO {

    private String openProdUrl;
    private String openSandboxUrl;

}
