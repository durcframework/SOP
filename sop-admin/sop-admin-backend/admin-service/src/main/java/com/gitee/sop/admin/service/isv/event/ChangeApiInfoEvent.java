package com.gitee.sop.admin.service.isv.event;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Collection;

/**
 * @author 六如
 */
@Getter
@AllArgsConstructor
public class ChangeApiInfoEvent {

    private final Collection<Long> apiIds;

}
