package com.gitee.sop.admin.service.doc.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author 六如
 */
@Data
public class DocInfoPublishUpdateDTO {

    @NotNull
    private Long id;

    /**
     * 1-发布,0-下线
     */
    private Integer isPublish;

}
