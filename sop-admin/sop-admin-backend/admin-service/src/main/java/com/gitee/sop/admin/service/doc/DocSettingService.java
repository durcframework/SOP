package com.gitee.sop.admin.service.doc;

import com.gitee.sop.admin.common.enums.ConfigKeyEnum;
import com.gitee.sop.admin.service.doc.dto.DocSettingDTO;
import com.gitee.sop.admin.service.sys.SysConfigService;
import com.gitee.sop.admin.service.sys.dto.SystemConfigDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;


/**
 * @author 六如
 */
@Service
public class DocSettingService {

    @Autowired
    private SysConfigService sysConfigService;

    public DocSettingDTO getDocSetting() {
        DocSettingDTO docSettingDTO = new DocSettingDTO();
        docSettingDTO.setTornaServerAddr(ConfigKeyEnum.TORNA_SERVER_ADDR.getValue());
        docSettingDTO.setOpenProdUrl(ConfigKeyEnum.OPEN_PROD_URL.getValue());
        docSettingDTO.setOpenSandboxUrl(ConfigKeyEnum.OPEN_SANDBOX_URL.getValue());
        return docSettingDTO;
    }

    public void save(DocSettingDTO docSettingDTO) {
        Collection<SystemConfigDTO> systemConfigDTOS = new ArrayList<>();
        systemConfigDTOS.add(new SystemConfigDTO(ConfigKeyEnum.TORNA_SERVER_ADDR.getKey(), docSettingDTO.getTornaServerAddr(), "Torna服务器地址"));
        systemConfigDTOS.add(new SystemConfigDTO(ConfigKeyEnum.OPEN_PROD_URL.getKey(), docSettingDTO.getOpenProdUrl(), "开放平台线上地址"));
        systemConfigDTOS.add(new SystemConfigDTO(ConfigKeyEnum.OPEN_SANDBOX_URL.getKey(), docSettingDTO.getOpenSandboxUrl(), "开放平台沙箱地址"));
        sysConfigService.save(systemConfigDTOS);
    }


}
