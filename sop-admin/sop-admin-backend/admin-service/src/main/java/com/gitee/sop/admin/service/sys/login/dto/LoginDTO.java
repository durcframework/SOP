package com.gitee.sop.admin.service.sys.login.dto;

import com.gitee.sop.admin.service.sys.login.enums.RegTypeEnum;
import lombok.Data;

@Data
public class LoginDTO {
    private String username;
    private String password;
    private RegTypeEnum regType = RegTypeEnum.BACKEND;
}
