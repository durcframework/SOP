package com.gitee.sop.admin.service.doc.dto.torna;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DocIdParam {

    private Long docId;
}
