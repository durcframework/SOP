package com.gitee.sop.admin.controller.isv.param;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;


/**
 * @author 六如
 */
@Data
public class IsvInfoAddParam {

    /**
     * 1启用，2禁用
     */
    @NotNull
    private Integer status;

    /**
     * 备注
     */
    @Length(max = 500)
    private String remark;

}
