package com.gitee.sop.admin.controller.serve.vo;

import com.gitee.sop.admin.service.jackson.convert.annotation.UserFormat;
import lombok.Data;

import java.time.LocalDateTime;


/**
 * @author 六如
 */
@Data
public class ApiInfoVO {

    /**
     * id
     */
    private Long id;

    /**
     * 所属应用
     */
    private String application;

    /**
     * 接口名称
     */
    private String apiName;

    /**
     * 版本号
     */
    private String apiVersion;

    /**
     * 接口描述
     */
    private String description;

    /**
     * 备注
     */
    private String remark;

    /**
     * 接口class
     */
    private String interfaceClassName;

    /**
     * 方法名称
     */
    private String methodName;

    /**
     * 参数信息
     */
    private String paramInfo;

    /**
     * 接口是否需要授权访问
     */
    private Integer isPermission;

    /**
     * 是否需要appAuthToken
     */
    private Integer isNeedToken;

    /**
     * 是否有公共响应参数
     */
    private Integer hasCommonResponse;

    /**
     * 注册来源，1-系统注册,2-手动注册
     */
    private Integer regSource;

    /**
     * 1启用，0禁用
     */
    private Integer status;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    @UserFormat
    private Long addBy;

    /**
     * 最后更新人id
     */
    @UserFormat
    private Long updateBy;

}
