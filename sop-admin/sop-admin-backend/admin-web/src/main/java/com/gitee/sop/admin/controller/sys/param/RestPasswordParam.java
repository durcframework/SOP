package com.gitee.sop.admin.controller.sys.param;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author 六如
 */
@Data
public class RestPasswordParam {

    @NotNull
    private Long userId;

    @NotBlank
    private String passwordHash;
}
