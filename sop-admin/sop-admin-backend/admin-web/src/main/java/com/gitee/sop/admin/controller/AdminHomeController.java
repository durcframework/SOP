package com.gitee.sop.admin.controller;

import com.gitee.sop.admin.common.annotation.NoToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@NoToken
public class AdminHomeController {

    private static final String REDIRECT_INDEX = "forward:index.html";

    // 后台admin入口地址
    @GetMapping("${index.path}")
    public String index() {
        return REDIRECT_INDEX;
    }

}
