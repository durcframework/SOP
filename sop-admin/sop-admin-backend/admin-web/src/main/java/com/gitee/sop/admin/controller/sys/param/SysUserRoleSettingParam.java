package com.gitee.sop.admin.controller.sys.param;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;


/**
 *
 * @author 六如
 */
@Data
public class SysUserRoleSettingParam {

    /**
     * sys_role.id
     */
    private List<Long> roleIds;

    /**
     * sys_user.id
     */
    @NotNull
    private Long userId;


}
