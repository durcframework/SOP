package com.gitee.sop.admin.controller.isv.param;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * @author 六如
 */
@Data
public class IsvInfoUpdateKeysParam {

    @NotNull
    private Long isvId;

    /**
     * 秘钥格式，1：PKCS8(JAVA适用)，2：PKCS1(非JAVA适用)
     */
    @NotNull
    private Integer keyFormat;

    /**
     * 开发者生成的公钥, 数据库字段：public_key_isv
     */
    @NotBlank
    private String publicKeyIsv;

    /**
     * 开发者生成的私钥（交给开发者）, 数据库字段：private_key_isv
     */
    @NotBlank
    private String privateKeyIsv;

    /**
     * 平台生成的公钥（交给开发者）, 数据库字段：public_key_platform
     */
    @NotBlank
    private String publicKeyPlatform;

    /**
     * 平台生成的私钥, 数据库字段：private_key_platform
     */
    @NotBlank
    private String privateKeyPlatform;
}
