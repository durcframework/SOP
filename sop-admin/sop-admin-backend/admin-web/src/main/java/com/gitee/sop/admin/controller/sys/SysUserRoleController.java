package com.gitee.sop.admin.controller.sys;

import com.gitee.sop.admin.common.context.UserContext;
import com.gitee.sop.admin.common.resp.Result;
import com.gitee.sop.admin.controller.sys.param.SysUserRoleSettingParam;
import com.gitee.sop.admin.service.sys.SysUserRoleService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 六如
 */
@RestController
@RequestMapping("sys/userrole")
public class SysUserRoleController {

    @Resource
    private SysUserRoleService sysUserRoleService;

    /**
     * 获取用户角色id
     *
     * @return 返回角色id
     */
    @GetMapping("/getUserRoleIds")
    public Result<List<Long>> getUserRoleIds(Long userId) {
        List<Long> roleIds = sysUserRoleService.listUserRoleIds(userId);
        return Result.ok(roleIds);
    }

    /**
     * 设置用户角色
     *
     * @param param 表单数据
     * @return 返回影响行数
     */
    @PostMapping("/setUserRole")
    public Result<Integer> setUserRole(@Validated @RequestBody SysUserRoleSettingParam param) {
        sysUserRoleService.setUserRole(param.getUserId(), param.getRoleIds(), UserContext.getUser());
        return Result.ok(1);
    }


}
