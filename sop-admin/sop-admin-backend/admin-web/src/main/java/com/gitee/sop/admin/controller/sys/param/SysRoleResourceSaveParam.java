package com.gitee.sop.admin.controller.sys.param;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author 六如
 */
@Data
public class SysRoleResourceSaveParam {

    /**
     * 角色id
     */
    @NotNull
    private Long roleId;

    /**
     * 资源id
     */
    @NotEmpty
    private List<Long> resourceIds;

}
