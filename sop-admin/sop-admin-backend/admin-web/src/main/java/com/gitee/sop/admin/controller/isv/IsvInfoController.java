package com.gitee.sop.admin.controller.isv;

import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.query.LambdaQuery;
import com.gitee.fastmybatis.core.query.param.PageParam;
import com.gitee.sop.admin.common.dto.StatusUpdateDTO;
import com.gitee.sop.admin.common.req.StatusUpdateParam;
import com.gitee.sop.admin.common.resp.Result;
import com.gitee.sop.admin.common.util.CopyUtil;
import com.gitee.sop.admin.common.util.RSATool;
import com.gitee.sop.admin.controller.isv.param.IsvInfoAddParam;
import com.gitee.sop.admin.controller.isv.param.IsvInfoUpdateKeysParam;
import com.gitee.sop.admin.controller.isv.param.IsvInfoUpdateParam;
import com.gitee.sop.admin.controller.isv.param.IsvKeysGenParam;
import com.gitee.sop.admin.controller.isv.vo.IsvInfoVO;
import com.gitee.sop.admin.dao.entity.IsvInfo;
import com.gitee.sop.admin.service.isv.IsvInfoService;
import com.gitee.sop.admin.service.isv.PermIsvGroupService;
import com.gitee.sop.admin.service.isv.dto.IsvInfoAddDTO;
import com.gitee.sop.admin.service.isv.dto.IsvInfoDTO;
import com.gitee.sop.admin.service.isv.dto.IsvInfoUpdateDTO;
import com.gitee.sop.admin.service.isv.dto.IsvInfoUpdateKeysDTO;
import com.gitee.sop.admin.service.isv.dto.IsvKeysDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author 六如
 */
@RestController
@RequestMapping("isv")
public class IsvInfoController {

    @Autowired
    private IsvInfoService isvInfoService;
    @Autowired
    private PermIsvGroupService permIsvGroupService;


    /**
     * 分页查询
     *
     * @param param
     * @return
     */
    @GetMapping("/page")
    public Result<PageInfo<IsvInfoVO>> page(PageParam param) {
        LambdaQuery<IsvInfo> query = param.toLambdaQuery(IsvInfo.class);
        PageInfo<IsvInfoDTO> isvInfoDTOPageInfo = isvInfoService.doPage(query);
        PageInfo<IsvInfoVO> retPage = isvInfoDTOPageInfo.convert(isvInfoDTO -> CopyUtil.copyBean(isvInfoDTO, IsvInfoVO::new));
        return Result.ok(retPage);
    }

    /**
     * 生成秘钥
     *
     * @param param
     * @return
     * @throws Exception
     */
    @PostMapping("createKeys")
    public Result<RSATool.KeyStore> createKeys(@Validated @RequestBody IsvKeysGenParam param) throws Exception {
        RSATool.KeyFormat format = RSATool.KeyFormat.of(param.getKeyFormat());
        RSATool.KeyStore keyStore = isvInfoService.createKeys(format);
        return Result.ok(keyStore);
    }

    /**
     * 获取秘钥信息
     *
     * @param isvId
     * @return
     */
    @GetMapping("/getKeys")
    public Result<IsvKeysDTO> getKeys(Long isvId) {
        IsvKeysDTO isvKeysDTO = isvInfoService.getKeys(isvId);
        return Result.ok(isvKeysDTO);
    }

    /**
     * 新增记录
     *
     * @param param
     * @return 返回添加后的主键值
     */
    @PostMapping("/add")
    public Result<Long> add(@Validated @RequestBody IsvInfoAddParam param) throws Exception {
        IsvInfoAddDTO isvInfoAddDTO = CopyUtil.copyBean(param, IsvInfoAddDTO::new);
        long id = isvInfoService.add(isvInfoAddDTO);
        // 返回添加后的主键值
        return Result.ok(id);
    }

    /**
     * 修改记录
     *
     * @param param 表单数据
     * @return 返回影响行数
     */
    @PostMapping("/update")
    public Result<Integer> update(@Validated @RequestBody IsvInfoUpdateParam param) {
        IsvInfoUpdateDTO isvInfoUpdateDTO = CopyUtil.copyBean(param, IsvInfoUpdateDTO::new);
        return Result.ok(isvInfoService.update(isvInfoUpdateDTO));
    }

    /**
     * 修改秘钥
     *
     * @param param 表单数据
     * @return 返回影响行数
     */
    @PostMapping("/updateKeys")
    public Result<Integer> updateKeys(@Validated @RequestBody IsvInfoUpdateKeysParam param) {
        IsvInfoUpdateKeysDTO isvInfoUpdateKeysDTO = CopyUtil.copyBean(param, IsvInfoUpdateKeysDTO::new);
        return Result.ok(isvInfoService.updateKeys(isvInfoUpdateKeysDTO));
    }

    /**
     * 修改状态
     *
     * @param param 表单数据
     * @return 返回影响行数
     */
    @PostMapping("/updateStatus")
    public Result<Integer> updateStatus(@Validated @RequestBody StatusUpdateParam param) {
        StatusUpdateDTO statusUpdateDTO = CopyUtil.copyBean(param, StatusUpdateDTO::new);
        return Result.ok(isvInfoService.updateStatus(statusUpdateDTO));
    }


}
