package com.gitee.sop.admin.controller.isv.param;

import com.gitee.fastmybatis.core.query.Operator;
import com.gitee.fastmybatis.core.query.annotation.Condition;
import com.gitee.fastmybatis.core.query.param.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 备注：分组表
 *
 * @author 六如
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PermGroupPageParam extends PageParam {

    /**
     * 分组描述
     */
    @Condition(operator = Operator.like)
    private String groupName;

}
