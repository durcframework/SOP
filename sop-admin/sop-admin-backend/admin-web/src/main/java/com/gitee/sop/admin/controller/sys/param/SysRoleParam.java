package com.gitee.sop.admin.controller.sys.param;

import com.gitee.fastmybatis.core.query.Operator;
import com.gitee.fastmybatis.core.query.annotation.Condition;
import com.gitee.fastmybatis.core.query.param.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 备注：角色表
 *
 * @author 六如
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SysRoleParam extends PageParam {
    private static final long serialVersionUID = 7794265174728302379L;

    /**
     * 角色名称
     */
    @Condition(operator = Operator.like)
    private String name;

    /**
     * 角色code
     */
    @Condition(operator = Operator.like)
    private String code;

    /**
     * 状态，1：启用，2：禁用
     */
    @Condition
    private Integer status;


}
