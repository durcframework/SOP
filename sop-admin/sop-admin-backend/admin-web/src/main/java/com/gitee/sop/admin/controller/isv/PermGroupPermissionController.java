package com.gitee.sop.admin.controller.isv;

import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.query.LambdaQuery;
import com.gitee.sop.admin.common.resp.Result;
import com.gitee.sop.admin.common.util.CopyUtil;
import com.gitee.sop.admin.controller.isv.param.PermGroupApiInfoParam;
import com.gitee.sop.admin.controller.isv.param.PermGroupPermissionParam;
import com.gitee.sop.admin.dao.entity.ApiInfo;
import com.gitee.sop.admin.service.isv.PermGroupPermissionService;
import com.gitee.sop.admin.service.isv.dto.PermGroupPermissionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 六如
 */
@RestController
@RequestMapping("perm/group/permission")
public class PermGroupPermissionController {

    @Autowired
    private PermGroupPermissionService permGroupPermissionService;

    /**
     * 查询分组接口
     *
     * @param param param
     * @return 返回分页结果
     */
    @GetMapping("/page")
    public Result<PageInfo<ApiInfo>> page(Long groupId, @Validated PermGroupApiInfoParam param) {
        LambdaQuery<ApiInfo> query = param.toLambdaQuery(ApiInfo.class);
        PageInfo<ApiInfo> apiInfoList = permGroupPermissionService.pageGroupApiId(groupId, query);
        return Result.ok(apiInfoList);
    }

    @PostMapping("setting")
    public Result<Integer> setting(@RequestBody @Validated PermGroupPermissionParam param) {
        PermGroupPermissionDTO permGroupPermissionDTO = CopyUtil.copyBean(param, PermGroupPermissionDTO::new);
        int cnt = permGroupPermissionService.setting(permGroupPermissionDTO);
        return Result.ok(cnt);
    }

    @PostMapping("delete")
    public Result<Integer> delete(@RequestBody @Validated PermGroupPermissionParam param) {
        int cnt = permGroupPermissionService.delete(param.getGroupId(), param.getApiIdList());
        return Result.ok(cnt);
    }


}
