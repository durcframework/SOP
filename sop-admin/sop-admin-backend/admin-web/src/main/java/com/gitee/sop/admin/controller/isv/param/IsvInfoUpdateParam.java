package com.gitee.sop.admin.controller.isv.param;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;


/**
 * @author 六如
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class IsvInfoUpdateParam extends IsvInfoAddParam {

    @NotNull
    private Long id;

}
