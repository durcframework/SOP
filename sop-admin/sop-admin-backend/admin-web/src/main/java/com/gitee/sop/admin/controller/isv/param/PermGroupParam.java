package com.gitee.sop.admin.controller.isv.param;

import com.gitee.fastmybatis.core.query.Operator;
import com.gitee.fastmybatis.core.query.annotation.Condition;
import com.gitee.fastmybatis.core.query.param.IParam;
import lombok.Data;

/**
 * @author 六如
 */
@Data
public class PermGroupParam implements IParam {

    /**
     * 分组描述
     */
    @Condition(operator = Operator.like)
    private String groupName;

}
