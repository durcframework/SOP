package com.gitee.sop.admin.controller.sys;

import com.gitee.sop.admin.common.resp.Result;
import com.gitee.sop.admin.controller.sys.param.ConfigSettingParam;
import com.gitee.sop.admin.service.sys.SysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 六如
 */
@RestController
@RequestMapping("sys/config")
public class SysConfigController {

    @Autowired
    private SysConfigService sysConfigService;

    /**
     * 保存配置
     *
     * @param param param
     */
    @PostMapping("/save")
    public Result<Integer> add(@Validated @RequestBody ConfigSettingParam param) {
        sysConfigService.save(param.getItems());
        return Result.ok(1);
    }


}
