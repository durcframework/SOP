package com.gitee.sop.admin.controller.isv;

import com.gitee.sop.admin.common.context.SpringContext;
import com.gitee.sop.admin.common.resp.Result;
import com.gitee.sop.admin.common.util.CopyUtil;
import com.gitee.sop.admin.controller.isv.param.IsvGroupSettingParam;
import com.gitee.sop.admin.service.isv.PermIsvGroupService;
import com.gitee.sop.admin.service.isv.dto.IsvGroupSettingDTO;
import com.gitee.sop.admin.service.isv.event.ChangeIsvPermEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

/**
 * @author 六如
 */
@RestController
@RequestMapping("perm/isv/group")
public class PermIsvGroupController {

    @Autowired
    private PermIsvGroupService permIsvGroupService;

    /**
     * 查询isv分组
     *
     * @param isvId isvId
     * @return 返回影响行数
     */
    @GetMapping("listIsvGroupId")
    public Result<List<Long>> listIsvGroupId(Long isvId) {
        List<Long> permGroups = permIsvGroupService.listIsvGroupId(isvId);
        return Result.ok(permGroups);
    }

    /**
     * 设置分组
     *
     * @param param 表单数据
     * @return 返回影响行数
     */
    @PostMapping("setting")
    public Result<Integer> updateIsvGroup(@Validated @RequestBody IsvGroupSettingParam param) {
        IsvGroupSettingDTO isvGroupSettingDTO = CopyUtil.copyBean(param, IsvGroupSettingDTO::new);
        int i = permIsvGroupService.updateIsvGroup(isvGroupSettingDTO);
        if (i > 0) {
            // 刷新isv权限
            SpringContext.publishEvent(new ChangeIsvPermEvent(Collections.singletonList(isvGroupSettingDTO.getIsvId())));
        }
        return Result.ok(i);
    }


}
