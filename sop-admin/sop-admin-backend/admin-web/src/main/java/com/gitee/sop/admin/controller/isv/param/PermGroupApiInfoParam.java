package com.gitee.sop.admin.controller.isv.param;

import com.gitee.fastmybatis.core.query.Operator;
import com.gitee.fastmybatis.core.query.annotation.Condition;
import com.gitee.fastmybatis.core.query.param.PageParam;
import lombok.Data;

/**
 * @author 六如
 */
@Data
public class PermGroupApiInfoParam extends PageParam {

    @Condition(operator = Operator.like)
    private String apiName;

    @Condition
    private Integer status;

    @Condition
    private Integer isPermission;
}
