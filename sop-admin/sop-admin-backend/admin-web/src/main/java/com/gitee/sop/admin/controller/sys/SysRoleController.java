package com.gitee.sop.admin.controller.sys;

import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.query.LambdaQuery;
import com.gitee.sop.admin.common.dto.StatusUpdateDTO;
import com.gitee.sop.admin.common.req.IdParam;
import com.gitee.sop.admin.common.req.StatusUpdateParam;
import com.gitee.sop.admin.common.resp.Result;
import com.gitee.sop.admin.common.util.CopyUtil;
import com.gitee.sop.admin.controller.sys.param.SysRoleParam;
import com.gitee.sop.admin.dao.entity.SysRole;
import com.gitee.sop.admin.service.sys.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 六如
 */
@RestController
@RequestMapping("sys/role")
public class SysRoleController {

    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 分页查询
     *
     * @param param 查询参数
     * @return 返回分页结果
     */
    @GetMapping("/page")
    public Result<PageInfo<SysRole>> page(SysRoleParam param) {
        LambdaQuery<SysRole> query = param.toLambdaQuery(SysRole.class);
        PageInfo<SysRole> pageInfo = sysRoleService.page(query);
        return Result.ok(pageInfo);
    }

    /**
     * 所有角色
     *
     * @return 返回分页结果
     */
    @GetMapping("/all")
    public Result<List<SysRole>> all() {
        List<SysRole> list = sysRoleService.listAll();
        return Result.ok(list);
    }

    /**
     * 新增记录
     *
     * @param sysRole 表单参数
     * @return 返回添加后的主键值
     */
    @PostMapping("/add")
    public Result<Long> add(@Validated @RequestBody SysRole sysRole) {
        sysRoleService.save(sysRole);
        // 返回添加后的主键值
        return Result.ok(sysRole.getId());
    }

    /**
     * 修改记录
     *
     * @param sysRole 表单数据
     * @return 返回影响行数
     */
    @PostMapping("/update")
    public Result<Integer> update(@Validated @RequestBody SysRole sysRole) {
        return Result.ok(sysRoleService.update(sysRole));
    }

    /**
     * 删除记录
     *
     * @param param 参数
     * @return 返回影响行数
     */
    @PostMapping("/delete")
    public Result<Integer> delete(@Validated @RequestBody IdParam param) {
        return Result.ok(sysRoleService.deleteById(param.getId()));
    }


    /**
     * 修改状态
     *
     * @param param 表单数据
     * @return 返回影响行数
     */
    @PostMapping("/updateStatus")
    public Result<Integer> updateStatus(@Validated @RequestBody StatusUpdateParam param) {
        StatusUpdateDTO statusUpdateDTO = CopyUtil.copyBean(param, StatusUpdateDTO::new);
        return Result.ok(sysRoleService.updateStatus(statusUpdateDTO));
    }


}
