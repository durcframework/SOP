package com.gitee.sop.admin.controller.sys.vo;

import lombok.Data;

import java.util.List;

/**
 * @author 六如
 */
@Data
public class LoginResultVO {
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 用户名
     */
    private String nickname;

    /**
     * 密码
     */
    private String password;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 注册类型
     */
    private Integer regType;

    /**
     * 状态，1：启用，0：禁用
     */
    private Integer status;

    private List<String> roles;

    private List<String> permissions;

    private String accessToken;

    private String refreshToken;

    private String expires;
}
