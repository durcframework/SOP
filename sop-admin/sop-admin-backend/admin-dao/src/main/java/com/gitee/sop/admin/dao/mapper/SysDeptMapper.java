package com.gitee.sop.admin.dao.mapper;

import com.gitee.fastmybatis.core.mapper.BaseMapper;
import com.gitee.sop.admin.dao.entity.SysDept;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 六如
 */
@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
