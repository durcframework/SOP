package com.gitee.sop.admin.dao.entity;

import java.time.LocalDateTime;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.PkStrategy;
import com.gitee.fastmybatis.annotation.Table;

import lombok.Data;


/**
 * 表名：doc_content
 * 备注：文档内容
 *
 * @author 六如
 */
@Table(name = "doc_content", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
@Data
public class DocContent {

    /**
     * id
     */
    private Long id;

    /**
     * doc_info.id
     */
    private Long docInfoId;

    /**
     * 文档内容
     */
    private String content;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    private Long addBy;

    /**
     * 修改人id
     */
    private Long updateBy;


}
