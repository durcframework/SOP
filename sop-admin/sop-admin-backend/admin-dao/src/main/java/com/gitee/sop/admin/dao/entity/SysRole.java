package com.gitee.sop.admin.dao.entity;

import java.time.LocalDateTime;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.PkStrategy;
import com.gitee.fastmybatis.annotation.Table;

import lombok.Data;


/**
 * 表名：sys_role
 * 备注：角色表
 *
 * @author 六如
 */
@Table(name = "sys_role", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
@Data
public class SysRole {

    /**
     * id
     */
    private Long id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色code
     */
    private String code;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态，1：启用，2：禁用
     */
    private Integer status;

    @com.gitee.fastmybatis.annotation.Column(logicDelete = true)
    private Integer isDeleted;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    private Long addBy;

    /**
     * 修改人id
     */
    private Long updateBy;


}
