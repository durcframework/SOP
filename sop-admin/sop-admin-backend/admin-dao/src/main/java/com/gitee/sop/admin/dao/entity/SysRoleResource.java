package com.gitee.sop.admin.dao.entity;

import java.time.LocalDateTime;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.PkStrategy;
import com.gitee.fastmybatis.annotation.Table;

import lombok.Data;


/**
 * 表名：sys_role_resource
 * 备注：角色资源关联表
 *
 * @author 六如
 */
@Table(name = "sys_role_resource", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
@Data
public class SysRoleResource {

    /**
     * id
     */
    private Long id;

    /**
     * sys_role.id
     */
    private Long roleId;

    /**
     * sys_resource.id
     */
    private Long resourceId;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    private Long addBy;

    /**
     * 修改人id
     */
    private Long updateBy;


}
