package com.gitee.sop.admin.dao.mapper;

import com.gitee.fastmybatis.core.mapper.BaseMapper;
import com.gitee.sop.admin.dao.entity.SysUserDept;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 六如
 */
@Mapper
public interface SysUserDeptMapper extends BaseMapper<SysUserDept> {

}
