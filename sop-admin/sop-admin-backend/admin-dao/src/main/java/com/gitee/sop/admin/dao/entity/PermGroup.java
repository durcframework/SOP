package com.gitee.sop.admin.dao.entity;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.PkStrategy;
import com.gitee.fastmybatis.annotation.Table;
import lombok.Data;

import java.time.LocalDateTime;


/**
 * 表名：perm_group
 * 备注：分组表
 *
 * @author 六如
 */
@Table(name = "perm_group", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
@Data
public class PermGroup {

    /**
     * id
     */
    private Long id;

    /**
     * 分组名称
     */
    private String groupName;

    /**
     * 是否删除
     */
    @com.gitee.fastmybatis.annotation.Column(logicDelete = true)
    private Integer isDeleted;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    private Long addBy;

    /**
     * 最后更新人id
     */
    private Long updateBy;
}
