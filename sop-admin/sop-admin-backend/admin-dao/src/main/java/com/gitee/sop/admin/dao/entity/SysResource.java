package com.gitee.sop.admin.dao.entity;

import java.time.LocalDateTime;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.PkStrategy;
import com.gitee.fastmybatis.annotation.Table;

import lombok.Data;


/**
 * 表名：sys_resource
 * 备注：菜单资源表
 *
 * @author 六如
 */
@Table(name = "sys_resource", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
@Data
public class SysResource {

    /**
     * id
     */
    private Long id;

    /**
     * 菜单类型（0代表菜单、1代表iframe、2代表外链、3代表按钮）
     */
    private Integer menuType;

    /**
     * 菜单名称
     */
    private String title;

    /**
     * 路由名称
     */
    private String name;

    /**
     * 路由路径
     */
    private String path;

    /**
     * 路由路径
     */
    private String component;

    /**
     * 排序
     */
    private Integer rank;

    /**
     * 路由重定向
     */
    private String redirect;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 右侧图标
     */
    private String extraIcon;

    /**
     * 进场动画（页面加载动画）
     */
    private String enterTransition;

    /**
     * 离场动画（页面加载动画）
     */
    private String leaveTransition;

    /**
     * 菜单激活
     */
    private String activePath;

    /**
     * 权限标识
     */
    private String auths;

    /**
     * 链接地址（需要内嵌的`iframe`链接地址）
     */
    private String frameSrc;

    /**
     * 加载动画（内嵌的`iframe`页面是否开启首次加载动画）
     */
    private Integer frameLoading;

    /**
     * 缓存页面
     */
    private Integer keepAlive;

    /**
     * 标签页（当前菜单名称或自定义信息禁止添加到标签页）
     */
    private Integer hiddenTag;

    /**
     * 固定标签页（当前菜单名称是否固定显示在标签页且不可关闭）
     */
    private Integer fixedTag;

    /**
     * 菜单（是否显示该菜单）
     */
    private Integer showLink;

    /**
     * 父级菜单（是否显示父级菜单
     */
    private Integer showParent;

    /**
     * 父级id
     */
    private Long parentId;

    /**
     * 是否删除
     */
    @com.gitee.fastmybatis.annotation.Column(logicDelete = true)
    private Integer isDeleted;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    private Long addBy;

    /**
     * 修改人id
     */
    private Long updateBy;


}
