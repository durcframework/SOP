package com.gitee.sop.admin.dao.mapper;

import com.gitee.fastmybatis.core.mapper.BaseMapper;
import com.gitee.sop.admin.dao.entity.SysConfig;

/**
 * @author 六如
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
