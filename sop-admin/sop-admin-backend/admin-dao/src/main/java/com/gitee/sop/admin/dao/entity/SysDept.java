package com.gitee.sop.admin.dao.entity;

import java.time.LocalDateTime;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.PkStrategy;
import com.gitee.fastmybatis.annotation.Table;

import lombok.Data;


/**
 * 表名：sys_dept
 * 备注：部门表
 *
 * @author 六如
 */
@Table(name = "sys_dept", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
@Data
public class SysDept {

    /**
     * id
     */
    private Long id;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态，1：启用，2：禁用
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 父级id
     */
    private Long parentId;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    private Long addBy;

    /**
     * 修改人id
     */
    private Long updateBy;


}
