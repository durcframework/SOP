package com.gitee.sop.admin.service;

import com.alibaba.fastjson2.JSON;
import com.gitee.sop.admin.BaseTest;
import com.gitee.sop.admin.service.sys.login.LoginService;
import com.gitee.sop.admin.service.sys.login.dto.LoginDTO;
import com.gitee.sop.admin.service.sys.login.dto.LoginUser;
import com.gitee.sop.admin.service.sys.login.enums.RegTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.util.Assert;


/**
 * @author 六如
 */
public class LoginServiceTest extends BaseTest {

    @Autowired
    LoginService loginService;

    @Test
    public void login() {
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUsername("admin");
        loginDTO.setPassword("123456");
        loginDTO.setRegType(RegTypeEnum.BACKEND);
        LoginUser loginUser = loginService.login(loginDTO);
        Assert.notNull(loginUser, "not null");
        System.out.println(JSON.toJSONString(loginUser));
    }

    @Test
    public void resetAdminPwd() {
        // 初始密码
        String defPassword = "123456";
        defPassword = DigestUtils.sha256Hex(defPassword);
        String encodedPassword = BCrypt.hashpw(defPassword, BCrypt.gensalt());
    }

}
