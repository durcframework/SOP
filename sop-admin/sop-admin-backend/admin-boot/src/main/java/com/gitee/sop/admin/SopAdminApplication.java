package com.gitee.sop.admin;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubbo
public class SopAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(SopAdminApplication.class, args);
    }

}
