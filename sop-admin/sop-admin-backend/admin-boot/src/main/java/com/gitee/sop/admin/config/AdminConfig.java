package com.gitee.sop.admin.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author 六如
 */
@Configuration
@ConfigurationProperties(prefix = "admin")
@Data
public class AdminConfig {

    private int jwtTimeoutDays;

}
