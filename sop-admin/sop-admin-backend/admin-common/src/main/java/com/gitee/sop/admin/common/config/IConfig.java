package com.gitee.sop.admin.common.config;

public interface IConfig {

    String getConfig(String key);

    String getConfig(String key, String defaultValue);

}
