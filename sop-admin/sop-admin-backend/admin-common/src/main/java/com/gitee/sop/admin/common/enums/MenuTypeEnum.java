package com.gitee.sop.admin.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 菜单类型
 * @author 六如
 */
@Getter
@AllArgsConstructor
public enum MenuTypeEnum implements IntEnum {
    MENU(0, "菜单"),
    IFRAME(1, "iframe"),
    LINK(2, "外链"),
    BUTTON(3, "按钮");

    private final Integer value;

    private final String description;
}
