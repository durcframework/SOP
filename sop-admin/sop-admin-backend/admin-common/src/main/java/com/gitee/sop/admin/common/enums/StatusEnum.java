package com.gitee.sop.admin.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * @author 六如
 */
@Getter
@AllArgsConstructor
public enum StatusEnum implements IntEnum {
    DISABLED(2, "禁用"),
    ENABLE(1, "启用"),
    SET_PWD(3, "重置密码");

    private final Integer value;
    private final String description;

    public static StatusEnum of(Integer value) {
        for (StatusEnum statusEnum : StatusEnum.values()) {
            if (Objects.equals(statusEnum.value, value)) {
                return statusEnum;
            }
        }
        return DISABLED;
    }


}
