package com.gitee.sop.admin.common.fill;

import com.gitee.fastmybatis.core.handler.FillType;

/**
 * 保存到数据库自动设置修改人
 * @author 六如
 */
public class UpdateByFill extends AddByFill {

    private static final String UPDATE_BY = "updateBy";

    @Override
    public FillType getFillType() {
        return FillType.UPDATE;
    }

    @Override
    protected String getTargetFieldName() {
        return UPDATE_BY;
    }
}
