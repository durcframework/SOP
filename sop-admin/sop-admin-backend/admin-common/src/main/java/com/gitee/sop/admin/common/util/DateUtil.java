package com.gitee.sop.admin.common.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author 六如
 */
public class DateUtil {

    static final DateTimeFormatter FORMATTER_FRONT = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    static final DateTimeFormatter FORMATTER_YMDHMS = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static String formatFrontDate(LocalDateTime localDateTime) {
        return FORMATTER_FRONT.format(localDateTime);
    }

    public static String formatYmdhms(LocalDateTime localDateTime) {
        return FORMATTER_YMDHMS.format(localDateTime);
    }

}
