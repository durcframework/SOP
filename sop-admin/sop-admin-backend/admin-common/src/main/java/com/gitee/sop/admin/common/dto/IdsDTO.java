package com.gitee.sop.admin.common.dto;

import lombok.Data;

import java.util.List;

/**
 * @author 六如
 */
@Data
public class IdsDTO {
    private List<Long> ids;
}
