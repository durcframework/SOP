package com.gitee.sop.admin.common.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author 六如
 */
@Data
public class StatusUpdateBatchDTO extends IdsDTO {

    @NotNull(message = "状态不能为空")
    private Integer status;

}
