package com.gitee.sop.admin.common.enums;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 六如
 */
public interface IntEnum extends IEnum<Integer> {

    static <T extends IntEnum> Optional<T> of(T[] values, Integer value) {
        for (IntEnum intEnum : values) {
            if (Objects.equals(intEnum.getValue(), value)) {
                return Optional.of((T) intEnum);
            }
        }
        return Optional.empty();
    }

    static <T extends IntEnum> Optional<String> ofDescription(T[] values, Integer value) {
        return of(values, value).map(IntEnum::getDescription);
    }

}
