package com.gitee.sop.admin.common.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author 六如
 */
@Data
public class StatusUpdateDTO extends IdDTO {

    @NotNull(message = "状态不能为空")
    private Integer status;

}
