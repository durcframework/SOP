package com.gitee.sop.admin.common.enums;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 六如
 */
public interface StringEnum extends IEnum<String> {

    static <T extends StringEnum> Optional<T> of(T[] values, String value) {
        for (StringEnum intEnum : values) {
            if (Objects.equals(intEnum.getValue(), value)) {
                return Optional.of((T) intEnum);
            }
        }
        return Optional.empty();
    }

    static <T extends StringEnum> Optional<String> ofDescription(T[] values, String value) {
        return of(values, value).map(StringEnum::getDescription);
    }

}
