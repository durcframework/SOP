package com.gitee.sop.admin.common.req;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author 六如
 */
@Data
public class IdsParam {
    @NotEmpty(message = "id不能为空")
    private List<Long> ids;
}
