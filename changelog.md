# changelog

## 日常更新

- 2025-03-12：优化dubbo filter
- 2025-03-09：优先使用本地缓存
- 2025-03-06：RouteContext新增isv对象
- 2025-03-05：变更拦截器方法参数
- 2025-03-04：拦截器新增init方法，用来做一些初始化工作
- 2025-02-27：新增token校验,com.gitee.sop.gateway.interceptor.internal.TokenValidateInterceptor.checkToken
- 2025-02-20：修复Linux环境下启动报错，加载i18n问题；优化新增接口注册保存逻辑
- 2025-02-19：升级fastmybatis到3.0.16
- 2025-02-09：优化Restful接口校验
- 2025-02-07：优化菜单排序
- 2025-02-04：新增Restful模式

## 5.1

接入smart-doc

## 5.0

全面重构,欢迎体验:[文档](https://www.yuque.com/u1604442/sop)
