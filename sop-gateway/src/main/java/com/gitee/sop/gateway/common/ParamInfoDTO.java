package com.gitee.sop.gateway.common;

import lombok.Data;

@Data
public class ParamInfoDTO {
    private String name;
    private String type;
    private String actualType;
}
