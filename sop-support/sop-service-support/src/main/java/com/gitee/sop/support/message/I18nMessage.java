package com.gitee.sop.support.message;

/**
 * 定义国际化消息
 *
 * @author 六如
 */
public interface I18nMessage {

    /**
     * i18n配置文件key
     */
    String getConfigKey();
}
