package com.gitee.sop.support.service;

import com.gitee.sop.support.service.dto.RegisterDTO;
import com.gitee.sop.support.service.dto.RegisterResult;

/**
 * @author 六如
 */
public interface ApiRegisterService {
    /**
     * 接口注册
     *
     * @param registerDTO 接口信息
     */
    RegisterResult register(RegisterDTO registerDTO);
}
