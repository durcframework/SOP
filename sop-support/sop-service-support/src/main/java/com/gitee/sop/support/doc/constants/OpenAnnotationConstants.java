package com.gitee.sop.support.doc.constants;

/**
 * @author 六如
 */
public class OpenAnnotationConstants {

    public static final String OPEN_ANNOTATION_NAME = "Open";

    public static final String PROP_VALUE = "value";

    public static final String PROP_VERSION = "version";
}
