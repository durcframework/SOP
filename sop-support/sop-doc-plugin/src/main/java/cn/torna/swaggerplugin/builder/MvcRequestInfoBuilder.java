package cn.torna.swaggerplugin.builder;

import cn.torna.swaggerplugin.bean.ControllerInfo;
import cn.torna.swaggerplugin.bean.TornaConfig;
import com.gitee.sop.support.annotation.Open;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.Method;

/**
 * @author tanghc
 */
public class MvcRequestInfoBuilder extends HttpMethodInfoBuilder {

    public MvcRequestInfoBuilder(ControllerInfo controllerInfo, Method method, TornaConfig tornaConfig) {
        super(controllerInfo, method, tornaConfig);
    }

    @Override
    public String buildUrl() {
        Method method = getMethod();
        Open open = AnnotationUtils.findAnnotation(method, Open.class);
        if (open == null) {
            throw new RuntimeException("接口[" + method + "]未定义@Open注解");
        }
        return open.value();
    }

    @Override
    public String getVersion() {
        Method method = getMethod();
        Open open = AnnotationUtils.findAnnotation(method, Open.class);
        if (open == null) {
            throw new RuntimeException("接口[" + method + "]未定义@Open注解");
        }
        return open.version();
    }

    @Override
    public String buildContentType() {
        return MediaType.APPLICATION_JSON_VALUE;
    }

    private String[] getConsumes() {
        RequestMapping requestMapping = AnnotatedElementUtils.findMergedAnnotation(getMethod(), RequestMapping.class);
        if (requestMapping != null) {
            return requestMapping.consumes();
        }
        return null;
    }

}
