-- 2025-02-09更新
-- api_info表新增api_mode字段
ALTER TABLE `api_info`
ADD COLUMN `api_mode` tinyint(4) NULL DEFAULT '1' COMMENT '接口模式，1-open接口，2-Restful模式' AFTER `reg_source`;
