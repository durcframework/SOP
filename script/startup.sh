#!/bin/sh

# 执行文件名称
app_name=${1}

if [ -z "${1}" ];then
  echo "未指定应用程序参数,格式:sh startup.sh xxx.jar"
  exit 0
fi

echo "启动程序:${app_name}"

# 先关闭服务
pid=$(ps -ef | grep $app_name | grep -v grep | grep -v .sh | awk '{print $2}')
if [ -n "$pid" ]; then
  echo "stop $app_name, pid:$pid"
  kill -9 "$pid"
fi


nohup java -server -jar -Dspring.profiles.active=test -Duser.timezone=Asia/Shanghai -Xms512m -Xmx512m $app_name >/dev/null 2>&1 &
