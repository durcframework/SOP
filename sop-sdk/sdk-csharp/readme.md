# sdk-csharp

C#对应的SDK，由于本人使用mac开发，因此使用了.Net Core

测试用例在`Program.cs`


## 接口封装步骤

比如获取故事信息接口

- 接口名：product.get
- 版本号：1.0
- 参数：id
- 返回信息

```
{
    "code": "0",
    "msg": "success",
    "sub_code": "",
    "sub_msg": "",
    "data": {
        "id": 1,
        "name": "冰 箱 -env=gray",
        "gmt_create": null
    }
}
```

针对这个接口，封装步骤如下：

1.在`Model`包下新建一个类，定义业务参数

```
namespace SDKCSharp.Model
{
    public class GetProductModel
    {

        /// <summary>
        /// id
        /// </summary>
        /// <value>The id.</value>
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
```

`[JsonProperty("name")]`是Newtonsoft.Json组件中的类，用于Json序列化，括号中的是参数名称。
类似于Java中的注解，`@JSONField(name = "xx")`

2.在`Response`包下新建一个返回类GetProductResponse

里面填写返回的字段

```
namespace SDKCSharp.Response
{
    public class GetProductResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("gmt_create")]
        public string GmtCreate { get; set; }

    }
}
```

3.在`Request`文件夹下新建一个请求类，继承`BaseRequest`

BaseRequest中有个泛型参数，填`GetStoryResponse`类，表示这个请求对应的返回类。
重写`GetMethod()`方法，填接口名。

如果要指定版本号，可重写`GetVersion()`方法，或者后续使用`request.Version = version`进行设置

```
namespace SDKCSharp.Request
{
    public class GetProductRequest : BaseRequest<GetProductResponse>
    {
        public override string GetMethod()
        {
            return "product.get";
        }
    }

}
```



## 使用方式

```
class MainClass
{
    static string url = "http://localhost:8081/api";
    static string appId = "2019032617262200001";
    // 平台提供的私钥
    static string privateKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCXJv1pQFqWNA/++OYEV7WYXwexZK/J8LY1OWlP9X0T6wHFOvxNKRvMkJ5544SbgsJpVcvRDPrcxmhPbi/sAhdO4x2PiPKIz9Yni2OtYCCeaiE056B+e1O2jXoLeXbfi9fPivJZkxH/tb4xfLkH3bA8ZAQnQsoXA0SguykMRZntF0TndUfvDrLqwhlR8r5iRdZLB6F8o8qXH6UPDfNEnf/K8wX5T4EB1b8x8QJ7Ua4GcIUqeUxGHdQpzNbJdaQvoi06lgccmL+PHzminkFYON7alj1CjDN833j7QMHdPtS9l7B67fOU/p2LAAkPMtoVBfxQt9aFj7B8rEhGCz02iJIBAgMBAAECggEARqOuIpY0v6WtJBfmR3lGIOOokLrhfJrGTLF8CiZMQha+SRJ7/wOLPlsH9SbjPlopyViTXCuYwbzn2tdABigkBHYXxpDV6CJZjzmRZ+FY3S/0POlTFElGojYUJ3CooWiVfyUMhdg5vSuOq0oCny53woFrf32zPHYGiKdvU5Djku1onbDU0Lw8w+5tguuEZ76kZ/lUcccGy5978FFmYpzY/65RHCpvLiLqYyWTtaNT1aQ/9pw4jX9HO9NfdJ9gYFK8r/2f36ZE4hxluAfeOXQfRC/WhPmiw/ReUhxPznG/WgKaa/OaRtAx3inbQ+JuCND7uuKeRe4osP2jLPHPP6AUwQKBgQDUNu3BkLoKaimjGOjCTAwtp71g1oo+k5/uEInAo7lyEwpV0EuUMwLA/HCqUgR4K9pyYV+Oyb8d6f0+Hz0BMD92I2pqlXrD7xV2WzDvyXM3s63NvorRooKcyfd9i6ccMjAyTR2qfLkxv0hlbBbsPHz4BbU63xhTJp3Ghi0/ey/1HQKBgQC2VsgqC6ykfSidZUNLmQZe3J0p/Qf9VLkfrQ+xaHapOs6AzDU2H2osuysqXTLJHsGfrwVaTs00ER2z8ljTJPBUtNtOLrwNRlvgdnzyVAKHfOgDBGwJgiwpeE9voB1oAV/mXqSaUWNnuwlOIhvQEBwekqNyWvhLqC7nCAIhj3yvNQKBgQCqYbeec56LAhWP903Zwcj9VvG7sESqXUhIkUqoOkuIBTWFFIm54QLTA1tJxDQGb98heoCIWf5x/A3xNI98RsqNBX5JON6qNWjb7/dobitti3t99v/ptDp9u8JTMC7penoryLKK0Ty3bkan95Kn9SC42YxaSghzqkt+uvfVQgiNGQKBgGxU6P2aDAt6VNwWosHSe+d2WWXt8IZBhO9d6dn0f7ORvcjmCqNKTNGgrkewMZEuVcliueJquR47IROdY8qmwqcBAN7Vg2K7r7CPlTKAWTRYMJxCT1Hi5gwJb+CZF3+IeYqsJk2NF2s0w5WJTE70k1BSvQsfIzAIDz2yE1oPHvwVAoGAA6e+xQkVH4fMEph55RJIZ5goI4Y76BSvt2N5OKZKd4HtaV+eIhM3SDsVYRLIm9ZquJHMiZQGyUGnsvrKL6AAVNK7eQZCRDk9KQz+0GKOGqku0nOZjUbAu6A2/vtXAaAuFSFx1rUQVVjFulLexkXR3KcztL1Qu2k5pB6Si0K/uwQ=";


    // 声明一个就行
    static OpenClient client = new OpenClient(url, appId, privateKey);

    public static void Main(string[] args)
    {
        TestGet();
    }

    // 标准用法
    private static void TestGet()
    {
        // 创建请求对象
        GetProductRequest request = new GetProductRequest();
        // 请求参数
        GetProductModel model = new GetProductModel();
        model.Id = 1;
        request.BizModel = model;

        // 发送请求
        Result<GetProductResponse> result = client.Execute(request);
      
        if (result.IsSuccess())
        {
            // 返回结果
            Console.WriteLine("成功！response:{0}\n响应原始内容:{1}", JsonUtil.ToJSONString(result), result.Data);
        }
        else
        {
            Console.WriteLine("错误, code:{0}, msg:{1}, subCode:{2}, subMsg:{3}",
                result.Code, result.Msg, result.SubCode, result.SubMsg);
        }
    }

    
}
```
