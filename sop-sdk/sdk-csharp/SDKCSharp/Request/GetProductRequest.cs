﻿using System;
using SDKCSharp.Common;
using SDKCSharp.Response;

namespace SDKCSharp.Request
{
    public class GetProductRequest : BaseRequest<GetProductResponse>
    {
        public override string GetMethod()
        {
            return "product.get";
        }
    }

}
