﻿using Newtonsoft.Json;

namespace SDKCSharp.Model
{
    public class GetProductModel
    {

        /// <summary>
        /// id
        /// </summary>
        /// <value>The id.</value>
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
