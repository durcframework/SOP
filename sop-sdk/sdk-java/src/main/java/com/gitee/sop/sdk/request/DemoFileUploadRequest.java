package com.gitee.sop.sdk.request;

import com.gitee.sop.sdk.response.GetProductResponse;

/**
 * @author 六如
 */
public class DemoFileUploadRequest extends BaseRequest<GetProductResponse> {
    @Override
    protected String method() {
        return "product.upload.more";
    }
}
