package com.gitee.sop.sdk.response;

import lombok.Data;

/**
 * @author 六如
 */
@Data
public class PayTradeWapPayResponse {

    private String pageRedirectionData;

}
