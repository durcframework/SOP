package com.gitee.sop.sdk.request;

import com.gitee.sop.sdk.response.GetProductResponse;

public class GetProductRequest extends BaseRequest<GetProductResponse> {
    @Override
    protected String method() {
        return "product.get";
    }

}
